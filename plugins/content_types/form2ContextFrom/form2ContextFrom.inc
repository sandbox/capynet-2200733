<?php
$plugin = array(
  'title' => t('form2ContextFrom'),
  'category' => t('Form'),
  'render callback' => 'form2ContextFrom_content_type_render',
);

/**
 * Render
 */
function form2ContextFrom_content_type_render($subtype, $conf, $panel_args, &$context) {

  $form = drupal_get_form("form2ContextFromForm"); //Está en este plugin mas abajo.
  $f = drupal_render($form);

  $block = new stdClass();
  $block->title = 'Filtra!';
  $block->content = $f;

  return $block;

}

/**
 * @return array FAPI form
 */
function form2ContextFromForm() {

  $form['nid'] = array(
    '#title' => t('un nid'),
    '#type' => 'textfield',
  );

  $form['lastname'] = array(
    '#title' => t('Un apellido'),
    '#type' => 'textfield',
  );

  $form['title'] = array(
    '#title' => t('Un title'),
    '#type' => 'textfield',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('¿Filtramos?'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function form2ContextFromForm_submit($form, &$form_state) {
  $data = new stdClass();
  $data->nid = ($form_state['values']["nid"] == "") ? NULL : $form_state['values']["nid"];
  $data->lastname = ($form_state['values']["lastname"] == "") ? NULL : $form_state['values']["lastname"];
  $data->title = ($form_state['values']["title"] == "") ? NULL : $form_state['values']["title"];

  // @todo implementar un nombre de caché unico, que 'form2Context' a secas va a colisionar si hay mas de una implementacion.
  cache_set("form2Context", serialize($data));
}
