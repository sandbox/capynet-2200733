<?php
$plugin = array(
  'title' => t("Form2Context"),
  'description' => t('transfiere informacion de un formulario a un contexto de panels.'),
  'context' => 'ctools_context_create_form2context',
  'context name' => 'form2context',
  'keyword' => 'form2context',
  'convert list' => 'form2context_list',
  'convert' => 'form2context_convert',
);

/**
 * Creador del contexto
 */
function ctools_context_create_form2context($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('form2context');
  $context->title = "Contextualized form";
  $context->plugin = 'form2context';

  $data = cache_get("form2Context");

  // @todo mejorar.
  if ($data === FALSE) {
    $context->data = new stdClass();
    $context->data->nid = NULL;
    $context->data->lastname = NULL;
    $context->data->title = NULL;
  }
  else {
    $context->data = unserialize($data->data);
    // @todo Ya podemos borrarlo?
  }

  return $context;
}

/**
 * Listado de tokens de los que dispone este contexto.
 * @see form2context_convert()
 */
function form2context_list() {
  return array(
    'nid' => t('Un nid'),
    'lastname' => t('Un lastname'),
    'title' => t('Un title'),
  );
}

/**
 * Render de los token del contexto.
 * @see form2context_list()
 */
function form2context_convert($context, $type) {
  switch ($type) {
    case 'nid':
    case 'lastname':
    case 'title':
      return $context->data->{$type};
  }
}